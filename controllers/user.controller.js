import CatchAsyncError from "../utils/catchAsyncError";
import ErrorHandler from "../utils/errorHandler";


export const registerUser = CatchAsyncError(
    async (req, res, next) => {
        try {
            
        } catch (error) {
            return next(new ErrorHandler(error.message, 400));
        }
    }
)