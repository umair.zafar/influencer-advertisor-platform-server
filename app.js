import cookieParser from "cookie-parser";
import express from "express";
import userRouter from "./routes/user.route";

export const app = express();

app.use(cookieParser());

app.use("/app/v1", userRouter);

app.get("/test", (req, res, next) => {
    res.status(200).json({
        success: true,
        message: "API is working",
    });
});

app.all("*", (req, res, next) => {
    const err = new Error(`Route ${req.originalUrl} not found!`);
    err.statusCode = 404;
    next(err);
});
